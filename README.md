[Tally](https://gitlab.com/nest.lbl.gov/tally) provides a
collection of interfaces, along with simple implementations, for
counting objects as they are pass through a your code. The
implementation can be easily changed as the circumstances dictate,
without requiring a recompilation of the your code.
