package gov.lbl.nest.tally;

import java.util.ServiceLoader;

/**
 * <p>
 * This abstract class defines the set of methods that can be used to obtain
 * instances of the monitoring interfaces.
 * </p>
 * <p/>
 * <p>
 * The {@link #getMonitorFactory()} method is used to obtain a concrete
 * implementation of this class. That implementation is resolved using the
 * mechanism laid out by {@link ServiceLoader} class.
 * </p>
 * <p>
 * Each request method has two forms, one with and one without the
 * <code>family</code> parameter. The <code>family</code> parameter is designed
 * to be a hint to the concrete factory about how to go about creating the
 * requested instance. The idea here is that some implementations may be able to
 * create different implementation of the same interface depending on the family
 * that is specified. For example a factory may associate one timeBase for rate
 * monitors in the "fast" family, while it associates a different timeBase for
 * those in the "slow" family. Of course, factories are free to ignore the
 * <code>family</code> parameter entirely.
 * 
 * @author patton
 */
public abstract class MonitorFactory {

    // public static final member data

    /**
     * The interface class for {@link ScalarRate} monitors.
     */
    public static final Class<ScalarFlow> SCALAR_FLOW = ScalarFlow.class;

    /**
     * The interface class for {@link ScalarLoad} monitors.
     */
    public static final Class<ScalarLoad> SCALAR_LOAD = ScalarLoad.class;

    /**
     * The interface class for {@link ScalarRate} monitors.
     */
    public static final Class<ScalarRate> SCALAR_RATE = ScalarRate.class;

    /**
     * The interface class for {@link ScalarSum} monitors.
     */
    public static final Class<ScalarSum> SCALAR_SUM = ScalarSum.class;

    /**
     * The interface class for {@link VectorFlow} monitors.
     */
    public static final Class<VectorFlow> VECTOR_FLOW = VectorFlow.class;

    /**
     * The interface class for {@link VectorRate} monitors.
     */
    public static final Class<VectorRate> VECTOR_RATE = VectorRate.class;

    /**
     * The interface class for {@link VectorSum} monitors.
     */
    public static final Class<VectorSum> VECTOR_SUM = VectorSum.class;

    // private static final member data

    /**
     * The {@link ServiceLoader} instance that will load the
     * {@link MonitorFactory} class.
     */
    private final static ServiceLoader<MonitorFactory> factoryLoader = ServiceLoader.load(MonitorFactory.class,
                                                                                          MonitorFactory.class.getClassLoader());

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns a new instance of the DefaultScalarFlow class created using the
     * current factory settings.
     * 
     * @param family
     *            the family with which to associate the new instance.
     * @return a new instance of the DefaultScalarFlow class.
     * @throws ClassNotFoundException
     *             if the factory can not provide either a {@link ScalarSum} or
     *             {@link ScalarRate} instance.
     */
    protected final ScalarFlow createScalarFlow(String family) throws ClassNotFoundException {
        final ScalarSum sum = newScalarMonitor(SCALAR_SUM,
                                               family);
        final ScalarRate rate = newScalarMonitor(SCALAR_RATE,
                                                 family);
        return new DefaultScalarFlow(sum,
                                     rate);
    }

    /**
     * Returns a new instance of the DefaultVectorFlow class created using the
     * current factory settings.
     * 
     * @param dimension
     *            the dimension of the vector to be monitored.
     * @param family
     *            the family with which to associate the new instance.
     * @return a new instance of the DefaultVectorFlow class.
     * @throws ClassNotFoundException
     *             if the factory can not provide {@link ScalarFlow} instances.
     */
    protected final VectorFlow createVectorFlow(int dimension,
                                                String family) throws ClassNotFoundException {
        final VectorSum sum = newVectorMonitor(VECTOR_SUM,
                                               dimension,
                                               family);
        final VectorRate rate = newVectorMonitor(VECTOR_RATE,
                                                 dimension,
                                                 family);
        return new DefaultVectorFlow(sum,
                                     rate);
    }

    /**
     * Returns a new instance of the DefaultVectorRate class created using the
     * current factory settings.
     * 
     * @param dimension
     *            the dimension of the vector to be monitored.
     * @param family
     *            the family with which to associate the new instance.
     * @return a new instance of the DefaultVectorRate class.
     * @throws ClassNotFoundException
     *             if the factory can not provide {@link ScalarSum} instances.
     */
    protected final VectorRate createVectorRate(int dimension,
                                                String family) throws ClassNotFoundException {
        final ScalarRate[] scalars = new ScalarRate[dimension];
        for (int scalar = 0; dimension != scalar; scalar++) {
            scalars[scalar] = (ScalarRate) newScalarMonitor(SCALAR_RATE);
        }
        return new DefaultVectorRate(scalars);
    }

    /**
     * Returns a new instance of the DefaultVectorSum class created using the
     * current factory settings.
     * 
     * @param dimension
     *            the dimension of the vector to be monitored.
     * @param family
     *            the family with which to associate the new instance.
     * @return a new instance of the DefaultVectorSum class.
     * @throws ClassNotFoundException
     *             if the factory can not provide {@link ScalarSum} instances.
     */
    protected final VectorSum createVectorSum(int dimension,
                                              String family) throws ClassNotFoundException {
        final ScalarSum[] scalars = new ScalarSum[dimension];
        for (int scalar = 0; dimension != scalar; scalar++) {
            scalars[scalar] = (ScalarSum) newScalarMonitor(SCALAR_SUM);
        }
        return new DefaultVectorSum(scalars);
    }

    /**
     * Returns a new instance of the specified interface. The specified class to
     * build should be one of the SCALAR_ constants of this class.
     * 
     * @param clazz
     *            the class of the interface this method should build.
     * @param <T>
     *            the interface this method should build.
     * @return a new ScalarMonitor instance.
     * @throws ClassNotFoundException
     *             if the factory can not provide an instance of specified
     *             interface.
     */
    public <T> T newScalarMonitor(Class<T> clazz) throws ClassNotFoundException {
        return newScalarMonitor(clazz,
                                null);
    }

    /**
     * Returns a new instance of the specified interface.
     * 
     * @param clazz
     *            the interface this method should build.
     * @param <T>
     *            the interface this method should build.
     * @param family
     *            the family with which to associate the new instance.
     * @return a new ScalarMonitor instance.
     * @throws ClassNotFoundException
     *             if the factory can not provide an instance of specified
     *             interface.
     */
    public abstract <T> T newScalarMonitor(Class<T> clazz,
                                           String family) throws ClassNotFoundException;

    /**
     * Returns a new instance of the specified interface.
     * 
     * @param clazz
     *            the interface this method should build.
     * @param <T>
     *            the interface this method should build.
     * @param dimension
     *            the dimension of the vector to be monitored.
     * @return a new VectorMonitor instance.
     * @throws ClassNotFoundException
     *             if the factory can not provide an instance of specified
     *             interface.
     */
    public <T> T newVectorMonitor(Class<T> clazz,
                                  int dimension) throws ClassNotFoundException {
        return newVectorMonitor(clazz,
                                dimension,
                                null);
    }

    /**
     * Returns a new instance of the specified interface.
     * 
     * @param clazz
     *            the interface this method should build.
     * @param <T>
     *            the interface this method should build.
     * @param dimension
     *            the dimension of the vector to be monitored.
     * @param family
     *            the family with which to associate the new instance.
     * @return a new VectorMonitor instance.
     * @throws ClassNotFoundException
     *             if the factory can not provide an instance of specified
     *             interface.
     */
    public abstract <T> T newVectorMonitor(Class<T> clazz,
                                           int dimension,
                                           String family) throws ClassNotFoundException;

    // static member methods (alphabetic)

    /**
     * Creates an instance of the {@link MonitorFactory} class.
     * 
     * @return the created instance of the {@link MonitorFactory} class.
     */
    public static MonitorFactory getMonitorFactory() {
        for (MonitorFactory factory : factoryLoader) {
            return factory;
        }
        return null;
    }

    // Description of this object.
    // public String toString() {}

}
