package gov.lbl.nest.tally;

import java.util.concurrent.TimeUnit;

/**
 * This class creates a simple implmentation of the {@link VectorFlow} interface
 * by wrapping a {@link VectorSum} instance and a {@link VectorRate} instance
 * into a single {@link VectorMonitor} instance. This is used by the
 * {@link MonitorFactory#createVectorFlow(int, String)})} method.
 * 
 * @author patton
 */
final class DefaultVectorFlow implements
                              VectorFlow {

    // private instance member data

    /**
     * The {@link DoubledUpVector} instance used to wrapped the components
     * objects.
     */
    private final DoubledUpVector monitor;

    // constructors

    /**
     * Create an instance of this class.
     * 
     * @param sum
     *            the {@link VectorSum} component of this object.
     * @param rate
     *            the {@link VectorRate} component of this object.
     */
    DefaultVectorFlow(VectorSum sum,
                      VectorRate rate) {
        monitor = new DoubledUpVector(sum,
                                      rate);
    }

    // instance member method (alphabetic)

    @Override
    public void dispose() {
        monitor.dispose();
    }

    @Override
    public int getDimension() {
        return monitor.getDimension();
    }

    @Override
    public float[] getRate() {
        return ((VectorRate) monitor.getSecond()).getRate();
    }

    @Override
    public TimeUnit getTimeUnit() {
        return ((VectorRate) monitor.getSecond()).getTimeUnit();
    }

    @Override
    public long[] getTotal() {
        return ((VectorSum) monitor.getFirst()).getTotal();
    }

    @Override
    public long[] getTotalSinceMark() {
        return ((VectorSum) monitor.getFirst()).getTotalSinceMark();
    }

    @Override
    public void mark() {
        ((VectorSum) monitor.getFirst()).mark();
    }

    @Override
    public void measure(int[] count) {
        monitor.measure(count);
    }

    @Override
    public void reset() {
        monitor.reset();
    }
}
