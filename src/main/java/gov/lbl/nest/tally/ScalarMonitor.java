package gov.lbl.nest.tally;

/**
 * This interface defines the set of methods that can be used to monitor a
 * single number.
 * 
 * @author patton
 */
public interface ScalarMonitor {

    // public static final member data

    /**
     * An implementation of this interface that does nothing.
     */
    ScalarMonitor NULL_MONITOR = new ScalarMonitor() {

        @Override
        public void dispose() {
        }

        @Override
        public void measure(int count) {
        }

        @Override
        public void reset() {
        }
    };

    // instance member method (alphabetic)

    /**
     * Tells this object is can release any resources it has been using.
     */
    void dispose();

    /**
     * Called to add a measurement to this object.
     * 
     * @param count
     *            the change in count since the last time this method was
     *            called.
     */
    void measure(int count);

    /**
     * Reset this object so it behaves as if it was just created.
     */
    void reset();
}
