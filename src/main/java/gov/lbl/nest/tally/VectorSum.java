package gov.lbl.nest.tally;

/**
 * This interface extends the {@link VectorMonitor} interface to enable
 * examination of the accumulated totals of a set of correlated tally points.
 * 
 * @author patton
 */
public interface VectorSum extends
                           VectorMonitor {

    /**
     * Returns the accumulated totals of all measurements.
     * 
     * @return the accumulated totals of all measurements.
     */
    long[] getTotal();

    /**
     * Returns the accumulated totals since the last mark was invoked.
     * 
     * @return the accumulated totals since the last mark was invoked.
     * @see #mark()
     */
    long[] getTotalSinceMark();

    /**
     * Mark this object's count.
     * 
     * @see #getTotalSinceMark()
     */
    void mark();
}