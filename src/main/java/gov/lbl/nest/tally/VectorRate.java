package gov.lbl.nest.tally;

import java.util.concurrent.TimeUnit;

/**
 * This interface extends the {@link VectorMonitor} interface to enable
 * examination of the rate of change of the totals is a set of correlated tally
 * points.
 * 
 * @author patton
 */
public interface VectorRate extends
                            VectorMonitor {
    /**
     * Returns the current measured rates.
     * 
     * @return the current measured rates.
     */
    float[] getRate();

    /**
     * The time base of the rate. (Note, that largest time base for the the JSEE
     * 5 compiled version of this interface is SECONDS, where JSEE6 allows
     * TimeUnits to be up to a DAY.)
     * 
     * @return he time base of the rate.
     */
    TimeUnit getTimeUnit();
}
