package gov.lbl.nest.tally;

/**
 * This interface defines the set of methods that can be used to monitor an
 * array of correlated numbers.
 * 
 * @author patton
 */
public interface VectorMonitor {

    // public static final member data

    /**
     * An implementation of this interface that does nothing.
     */
    VectorMonitor NULL_MONITOR = new VectorMonitor() {

        private int dimension;

        @Override
        public void dispose() {
        }

        @Override
        public int getDimension() {
            return dimension;
        }

        @Override
        public void measure(int[] counts) {
            if (0 == dimension) {
                dimension = counts.length;
            } else {
                if (dimension != counts.length) {
                    throw new IllegalArgumentException("The count array" + " should have a"
                                                       + " length of "
                                                       + dimension
                                                       + " not "
                                                       + counts.length
                                                       + '.');
                }
            }
        }

        @Override
        public void reset() {
        }
    };

    // instance member method (alphabetic)

    /**
     * Tells this object is can release any resources it has been using.
     */
    void dispose();

    /**
     * Returns the length of the array expected in a {@link #measure} call.
     * 
     * @return the length of the array expected in a {@link #measure} call.
     */
    int getDimension();

    /**
     * Called to add a measurement to this object.
     * 
     * @param counts
     *            the change in counts since the last time this method was
     *            called.
     */
    void measure(int[] counts);

    /**
     * Reset this object so it behaves as if it was just created.
     */
    void reset();
}
