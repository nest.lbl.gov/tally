package gov.lbl.nest.tally;

import java.util.concurrent.TimeUnit;

/**
 * This interface extends the {@link ScalarMonitor} interface to enable
 * examination of the rate of change of the total of a single tally point.
 * 
 * @author patton
 */
public interface ScalarRate extends
                            ScalarMonitor {
    /**
     * Returns current the rate of change of the total of a single tally point.
     * 
     * @return current the rate of change of the total of a single tally point.
     */
    float getRate();

    /**
     * The time base of the rate. (Note, that largest time base for the the JSEE
     * 5 compiled version of this interface is SECONDS, where JSEE6 allows
     * TimeUnits to be up to a DAY.)
     * 
     * @return he time base of the rate.
     */
    TimeUnit getTimeUnit();
}