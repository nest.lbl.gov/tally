/**
 * The Tally package as a simple set of interfaces that can be used to measure
 * any objects that your Java code may be handling. It is distributed with a set
 * of simple implementation of these interfaces, but it is easy to create and
 * use your own implementations.
 */
package gov.lbl.nest.tally;
