package gov.lbl.nest.tally;

/**
 * This class wraps up two {@link VectorMonitor} instances so that they appear
 * as a single VectorMonitor.
 * 
 * @author patton
 */
public class DoubledUpVector implements
                             VectorMonitor {

    // private instance member data

    /**
     * The first VectorMonitor wrapped by this class.
     */
    private final VectorMonitor first;

    /**
     * The second VectorMonitor wrapped by this class.
     */
    private final VectorMonitor second;

    // constructors

    /**
     * Create an instance of this class.
     * 
     * @param first
     *            one of the monitors to be used by this object.
     * @param second
     *            the other monitor to be used by this object.
     */
    public DoubledUpVector(VectorMonitor first,
                           VectorMonitor second) {
        if (first.getDimension() != second.getDimension()) {
            throw new IllegalArgumentException("Both VectorMonitor objects" + " must has the same dimension");
        }
        this.first = first;
        this.second = second;
    }

    // instance member method (alphabetic)

    @Override
    public void dispose() {
        first.dispose();
        second.dispose();
    }

    @Override
    public int getDimension() {
        return first.getDimension();
    }

    /**
     * Returns the first VectorMonitor wrapped by this class.
     * 
     * @return the first VectorMonitor wrapped by this class.
     */
    public VectorMonitor getFirst() {
        return first;
    }

    /**
     * Returns the second VectorMonitor wrapped by this class.
     * 
     * @return the second VectorMonitor wrapped by this class.
     */
    public VectorMonitor getSecond() {
        return second;
    }

    @Override
    public void measure(int[] count) {
        first.measure(count);
        second.measure(count);
    }

    @Override
    public void reset() {
        first.reset();
        second.reset();
    }
}
