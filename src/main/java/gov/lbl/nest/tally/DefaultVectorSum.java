package gov.lbl.nest.tally;

/**
 * This class creates a simple implmentation of the {@link VectorSum} interface
 * by wrapping the approapriate number of {@link ScalarSum} instances into a
 * single {@link VectorMonitor} instance. This is used by the
 * {@link MonitorFactory#createVectorRate(int, String)} method.
 * 
 * @author patton
 */
class DefaultVectorSum implements
                       VectorSum {
    // private instance member data

    /**
     * The array of {@link ScalarSum} instances used by this object.
     */
    private final ScalarSum[] scalars;

    // constructors

    /**
     * Create an instance of this class.
     * 
     * @param scalars
     *            the array of {@link ScalarSum} instances to be used by this
     *            object.
     */
    DefaultVectorSum(ScalarSum[] scalars) {
        this.scalars = scalars;
    }

    // instance member method (alphabetic)

    @Override
    public void dispose() {
        final int finished = getDimension();
        for (int scalar = 0; finished != scalar; scalar++) {
            scalars[scalar].dispose();
        }
    }

    @Override
    public int getDimension() {
        return scalars.length;
    }

    @Override
    public void measure(int[] counts) {
        final int finished = getDimension();
        if (finished != counts.length) {
            throw new IllegalArgumentException("\"counts\" has a length of " + counts.length
                                               + ", but required length is "
                                               + finished);
        }

        for (int scalar = 0; finished != scalar; scalar++) {
            scalars[scalar].measure(counts[scalar]);
        }
    }

    @Override
    public long[] getTotal() {
        final int finished = scalars.length;
        final long[] result = new long[finished];
        for (int scalar = 0; finished != scalar; scalar++) {
            result[scalar] = scalars[scalar].getTotal();
        }
        return result;
    }

    @Override
    public long[] getTotalSinceMark() {
        final int finished = scalars.length;
        final long[] result = new long[finished];
        for (int scalar = 0; finished != scalar; scalar++) {
            result[scalar] = scalars[scalar].getTotalSinceMark();
        }
        return result;
    }

    @Override
    public void reset() {
        final int finished = scalars.length;
        for (int scalar = 0; finished != scalar; scalar++) {
            scalars[scalar].reset();
        }
    }

    @Override
    public void mark() {
        final int finished = scalars.length;
        for (int scalar = 0; finished != scalar; scalar++) {
            scalars[scalar].mark();
        }
    }
}
