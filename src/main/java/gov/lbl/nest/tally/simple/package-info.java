/**
 * This package contains simple implementation of all of the monitor interfaces
 * defined in {@link gov.lbl.nest.tally}.
 */
package gov.lbl.nest.tally.simple;