package gov.lbl.nest.tally.simple;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import gov.lbl.nest.tally.ScalarRate;

/**
 * This class is a simple implementation of the {@link ScalarRate} interface.
 * 
 * @author patton
 */
public class ScalarRateImpl implements
                            ScalarRate {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The current change in the count total during the interval.
     */
    private long deltaTotal;

    /**
     * The list of measurements that are contained in the {@link #deltaTotal}.
     */
    private final List<Measurement> history = new LinkedList<Measurement>();

    /**
     * The interval, in milliseconds, over which to measure the change in
     * counts.
     */
    private final long interval;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param interval
     *            the number of seconds over which to measure the rate.
     */
    ScalarRateImpl(int interval) {
        if (0 >= interval) {
            throw new IllegalArgumentException("interval must be greater or equal to 1");
        }
        this.interval = TimeUnit.MILLISECONDS.convert(interval,
                                                      TimeUnit.SECONDS);
    }

    // instance member method (alphabetic)

    @Override
    public float getRate() {
        updateInterval(System.currentTimeMillis());
        return ((float) TimeUnit.MILLISECONDS.convert(deltaTotal,
                                                      TimeUnit.SECONDS))
               / interval;
    }

    @Override
    public TimeUnit getTimeUnit() {
        return TimeUnit.SECONDS;
    }

    @Override
    public void dispose() {
    }

    @Override
    public void measure(int count) {
        final long now = System.currentTimeMillis();
        updateInterval(now);
        history.add(new Measurement(now,
                                    count));
        deltaTotal += count;
    }

    @Override
    public void reset() {
        deltaTotal = 0;
        history.clear();
    }

    /**
     * Updates {@link #deltaTotal} and {@link #history} so that they only cover
     * the interval specified by the supplied "now".
     * 
     * @param now
     *            the time, in milliseconds, that is the new end of the
     *            interval.
     */
    private void updateInterval(long now) {
        final long cutOff = now - interval;
        final Iterator<Measurement> iterator = history.iterator();
        int index = 0;
        while (iterator.hasNext()) {
            final Measurement measurement = iterator.next();
            if (measurement.time < cutOff) {
                deltaTotal -= measurement.value;
                ++index;
            } else {
                final List<Measurement> excess = history.subList(0,
                                                                 index);
                excess.clear();
                return;
            }
        }
        // All history has been used.
        reset();
    }

    // static member methods (alphabetic)

    /**
     * This class captures a single measurement.
     */
    private class Measurement {

        /**
         * The time the measurement was made.
         */
        private final long time;

        /**
         * The value of the measurement.
         */
        private final int value;

        /**
         * Creates an instance of this class.
         * 
         * @param time
         *            the time the measurement was made.
         * @param value
         *            the value of the measurement.
         */
        private Measurement(long time,
                            int value) {
            this.time = time;
            this.value = value;
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
