package gov.lbl.nest.tally.simple;

import gov.lbl.nest.tally.MonitorFactory;

/**
 * This class manages provides a set of simple implementations to the standard
 * monitoring classes.
 * 
 * @author patton
 */
public class SimpleMonitorFactory extends
                                  MonitorFactory {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The default interval over which to measure changes in totals.
     */
    private static final int DEFAULT_RATE_INTERVAL = 10;

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @SuppressWarnings("unchecked")
    @Override
    public <T> T newScalarMonitor(Class<T> clazz,
                                  String family) throws ClassNotFoundException {
        if (SCALAR_FLOW.equals(clazz)) {
            return (T) createScalarFlow(family);
        }
        if (SCALAR_LOAD.equals(clazz)) {
            final Class<?> klass = this.getClass();
            final String name = klass.getName() + ".load.interval";
            final String intervalAsString = System.getProperty(name);
            int interval = 0;
            if (null != intervalAsString) {
                try {
                    interval = Integer.parseInt(intervalAsString);
                } catch (NumberFormatException e) {
                    // Do nothing, use default interval.
                }
            }
            return (T) new ScalarLoadImpl(interval);
        }
        if (SCALAR_RATE.equals(clazz)) {
            final Class<?> klass = this.getClass();
            final String name = klass.getName() + ".rate.interval";
            final String intervalAsString = System.getProperty(name);
            int interval = DEFAULT_RATE_INTERVAL;
            if (null != intervalAsString) {
                try {
                    interval = Integer.parseInt(intervalAsString);
                } catch (NumberFormatException e) {
                    // Do nothing, use default interval.
                }
            }
            return (T) new ScalarRateImpl(interval);
        }
        if (SCALAR_SUM.equals(clazz)) {
            return (T) new ScalarSumImpl();
        }
        throw new ClassNotFoundException("This MonitorFactory does not " + "support the"
                                         + clazz.toString()
                                         + " interface.");
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T newVectorMonitor(Class<T> clazz,
                                  int dimension,
                                  String family) throws ClassNotFoundException {
        if (VECTOR_FLOW.equals(clazz)) {
            return (T) createVectorFlow(dimension,
                                        family);
        }
        if (VECTOR_RATE.equals(clazz)) {
            return (T) createVectorRate(dimension,
                                        family);
        }
        if (VECTOR_SUM.equals(clazz)) {
            return (T) createVectorSum(dimension,
                                       family);
        }
        throw new ClassNotFoundException("This MonitorFactory does not " + "support the"
                                         + clazz.toString()
                                         + " interface.");
    }

    // static member methods (alphabetic)

    // Description of this object.
    // public String toString() {}

    // public static void main(String args[]) {}
}