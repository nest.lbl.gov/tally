package gov.lbl.nest.tally.simple;

import gov.lbl.nest.tally.ScalarSum;

/**
 * This class is a simple implementation of the {@link ScalarSum} interface.
 * 
 * @author patton
 */
class ScalarSumImpl implements
                    ScalarSum {

    // private instance member data

    /**
     * The accumulated total up until the last mark was invoked.
     */
    private long mark;

    /**
     * The accumulated total of all measurements.
     */
    private long total;

    // constructors

    /**
     * Create an instance of this class.
     */
    ScalarSumImpl() {
    }

    // instance member method (alphabetic)

    @Override
    public void dispose() {
    }

    @Override
    public synchronized long getTotal() {
        return total;
    }

    @Override
    public long getTotalSinceMark() {
        return total - mark;
    }

    @Override
    public synchronized void measure(int count) {
        total += count;
    }

    @Override
    public void mark() {
        mark = total;
    }

    @Override
    public synchronized void reset() {
        total = 0;
        mark();
    }
}