package gov.lbl.nest.tally.simple;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import gov.lbl.nest.tally.ScalarLoad;

/**
 * This class is a simple implementation of the {@link ScalarLoad} interface.
 * 
 * @author patton
 */
public class ScalarLoadImpl implements
                            ScalarLoad {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The most recent measurement of the tally point.
     */
    private int lastValue;

    /**
     * The current time weighted measurement during the interval.
     */
    private long deltaArea;

    /**
     * The list of measurements that are contained in the {@link #deltaArea}.
     */
    private final List<Measurement> history = new LinkedList<Measurement>();

    /**
     * The interval, in milliseconds, over which to average the measurement.
     */
    private final long interval;

    /**
     * The time, in milliseconds, when this instance was created.
     */
    private Long start;

    /**
     * The time, in milliseconds, that {@link #deltaArea} was updated.
     */
    private long then;

    // constructors

    /**
     * Creates an instance of this class. If is equivalent to
     * ScalarAverageImpl(0).
     */
    ScalarLoadImpl() {
        this(0);
    }

    /**
     * Creates an instance of this class. If the interval is less or equal to
     * zero the the average is over the lifetime (or since reset) of this
     * instance.
     * 
     * @param interval
     *            the number of seconds over which to measure the average.
     */
    ScalarLoadImpl(int interval) {
        if (0 >= interval) {
            start = Long.valueOf(System.currentTimeMillis());
            this.interval = 0;
            return;
        }
        this.interval = TimeUnit.MILLISECONDS.convert(interval,
                                                      TimeUnit.SECONDS);
    }

    // instance member method (alphabetic)

    @Override
    public int getLast() {
        return lastValue;
    }

    @Override
    public float getLoad() {
        long now = System.currentTimeMillis();
        updateInterval(now);
        if (null != start) {
            return ((float) deltaArea) / (now - start.longValue());
        }
        return ((float) deltaArea) / interval;
    }

    @Override
    public void dispose() {
    }

    @Override
    public void measure(int count) {
        long now = System.currentTimeMillis();
        if (updateInterval(now)) {
            history.add(new Measurement(now,
                                        count));
        }
        lastValue = count;
    }

    @Override
    public void reset() {
        lastValue = 0;
        deltaArea = 0;
        history.clear();
        if (null != start) {
            start = Long.valueOf(System.currentTimeMillis());
        }
        then = 0;
    }

    /**
     * Updates {@link #deltaArea} and {@link #history} so that they only over
     * the interval specified by the supplied "now".
     * 
     * @param now
     *            the time, in milliseconds, that is the new end of the
     *            interval.
     * 
     * @return true when the {@link #history} needs to be updated, i.e. the
     *         interval is limited.
     */
    private boolean updateInterval(long now) {
        if (null != start) {
            // Add new area since last update
            if (0 != then) {
                deltaArea += lastValue * (now - then);
            }
            then = now;
            return false;
        }
        final Iterator<Measurement> iterator = history.iterator();
        if (!iterator.hasNext()) {
            deltaArea = lastValue * interval;
            then = now;
            return true;
        }

        // Add new area since last update
        if (0 != then) {
            deltaArea += lastValue * (now - then);
        }
        then = now;

        final long cutOff = now - interval;
        Measurement begin = iterator.next();
        if (cutOff <= begin.time) {
            // There is nothing to be trimmed from the history.
            return true;
        }
        int index = 0;
        boolean outsideInterval = true;
        while (outsideInterval && iterator.hasNext()) {
            final Measurement end = iterator.next();
            if (cutOff > end.time) {
                deltaArea -= begin.value * (end.time - begin.time);
                begin = end;
                ++index;
            } else {
                outsideInterval = false;
            }
        }

        final List<Measurement> excess = history.subList(0,
                                                         index);
        excess.clear();
        if (1 == history.size()) {
            deltaArea = lastValue * interval;
        } else {
            deltaArea -= begin.value * (cutOff - begin.time);
        }
        begin.time = cutOff;
        return true;
    }

    // static member methods (alphabetic)

    /**
     * This class captures a single measurement.
     */
    private class Measurement {

        /**
         * The time the measurement was made.
         */
        private long time;

        /**
         * The value of the measurement.
         */
        private final int value;

        /**
         * Creates an instance of this class.
         * 
         * @param time
         *            the time the measurement was made.
         * @param value
         *            the value of the measurement.
         */
        private Measurement(long time,
                            int value) {
            this.time = time;
            this.value = value;
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
