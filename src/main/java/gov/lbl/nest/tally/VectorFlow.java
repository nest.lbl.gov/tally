package gov.lbl.nest.tally;

import java.util.concurrent.TimeUnit;

/**
 * This interface combines the Sum and Rate interfaces to define a single
 * {@link VectorMonitor} object that can be used to examination of the "flow" of
 * a set of corrolated tally points.
 * 
 * @author patton
 */
public interface VectorFlow extends
                            VectorSum,
                            VectorRate {

    // public static final member data

    /**
     * An implementation of this interface that does nothing.
     */
    public static final VectorFlow NULL_MONITOR = new VectorFlow() {

        @Override
        public void dispose() {
        }

        @Override
        public int getDimension() {
            throw new UnsupportedOperationException("This is the NULL monitor");
        }

        @Override
        public float[] getRate() {
            throw new UnsupportedOperationException("This is the NULL monitor");
        }

        @Override
        public TimeUnit getTimeUnit() {
            throw new UnsupportedOperationException("This is the NULL monitor");
        }

        @Override
        public long[] getTotal() {
            throw new UnsupportedOperationException("This is the NULL monitor");
        }

        @Override
        public long[] getTotalSinceMark() {
            throw new UnsupportedOperationException("This is the NULL monitor");
        }

        @Override
        public void mark() {
        }

        @Override
        public void measure(int[] count) {
        }

        @Override
        public void reset() {
        }
    };
}
