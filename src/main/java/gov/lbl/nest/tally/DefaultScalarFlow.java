package gov.lbl.nest.tally;

import java.util.concurrent.TimeUnit;

/**
 * This class creates a simple implementation of the {@link ScalarFlow}
 * interface by wrapping a {@link ScalarSum} instance and a {@link ScalarRate}
 * instance into a single {@link ScalarMonitor} instance. This is used by the
 * {@link MonitorFactory#createScalarFlow(String)} method.
 * 
 * @author patton
 */
final class DefaultScalarFlow implements
                              ScalarFlow {

    // private instance member data

    /**
     * The {@link DoubledUpScalar} instance used to wrapped the components
     * objects.
     */
    private final DoubledUpScalar monitor;

    // constructors

    /**
     * Create an instance of this class.
     * 
     * @param sum
     *            the {@link ScalarSum} component of this object.
     * @param rate
     *            the {@link ScalarRate} component of this object.
     */
    DefaultScalarFlow(ScalarSum sum,
                      ScalarRate rate) {
        monitor = new DoubledUpScalar(sum,
                                      rate);
    }

    // instance member method (alphabetic)

    @Override
    public void dispose() {
        monitor.dispose();
    }

    @Override
    public float getRate() {
        return ((ScalarRate) monitor.getSecond()).getRate();
    }

    @Override
    public TimeUnit getTimeUnit() {
        return ((ScalarRate) monitor.getSecond()).getTimeUnit();
    }

    @Override
    public long getTotal() {
        return ((ScalarSum) monitor.getFirst()).getTotal();
    }

    @Override
    public long getTotalSinceMark() {
        return ((ScalarSum) monitor.getFirst()).getTotalSinceMark();
    }

    @Override
    public void mark() {
        ((ScalarSum) monitor.getFirst()).mark();
    }

    @Override
    public void measure(int count) {
        monitor.measure(count);
    }

    @Override
    public void reset() {
        monitor.reset();
    }
}
