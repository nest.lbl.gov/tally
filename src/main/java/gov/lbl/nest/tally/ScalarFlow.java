package gov.lbl.nest.tally;

import java.util.concurrent.TimeUnit;

/**
 * This interface combines the Sum and Rate interfaces to define a single
 * {@link ScalarMonitor} object that can be used to examination of the "flow" of
 * a single tally point.
 * 
 * @author patton
 */
public interface ScalarFlow extends
                            ScalarSum,
                            ScalarRate {

    // public static final member data

    /**
     * An implementation of this interface that does nothing.
     */
    public static final ScalarFlow NULL_MONITOR = new ScalarFlow() {

        @Override
        public void dispose() {
        }

        @Override
        public float getRate() {
            throw new UnsupportedOperationException("This is the NULL monitor");
        }

        @Override
        public TimeUnit getTimeUnit() {
            return TimeUnit.SECONDS;
        }

        @Override
        public long getTotal() {
            throw new UnsupportedOperationException("This is the NULL monitor");
        }

        @Override
        public long getTotalSinceMark() {
            throw new UnsupportedOperationException("This is the NULL monitor");
        }

        @Override
        public void mark() {
        }

        @Override
        public void measure(int count) {
        }

        @Override
        public void reset() {
        }
    };
}
