package gov.lbl.nest.tally;

/**
 * This interface extends the {@link ScalarMonitor} interface to enable
 * examination of the time weighted average of the measurement of a single tally
 * point.
 * 
 * @author patton
 */
public interface ScalarLoad extends
                            ScalarMonitor {
    /**
     * Returns the last measurement of the tally point.
     * 
     * @return the last measurement of the tally point.
     */
    int getLast();

    /**
     * Returns current time weighted average of the measurement of a single
     * tally point.
     * 
     * @return current time weighted average of the measurement of a single
     *         tally point.
     */
    float getLoad();
}