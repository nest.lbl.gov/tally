package gov.lbl.nest.tally;

import java.util.concurrent.TimeUnit;

/**
 * This class creates a simple implementation of the {@link VectorRate}
 * interface by wrapping the appropriate number of {@link ScalarRate} instances
 * into a single {@link VectorMonitor} instance. This is used by the
 * {@link MonitorFactory#createVectorRate(int, String)} method.
 * 
 * @author patton
 */
class DefaultVectorRate implements
                        VectorRate {
    // private instance member data

    /**
     * The array of {@link ScalarSum} instances used by this object.
     */
    private final ScalarRate[] scalars;

    // constructors

    /**
     * Create an instance of this class.
     * 
     * @param scalars
     *            the array of {@link ScalarRate} instances to be used by this
     *            object.
     */
    DefaultVectorRate(ScalarRate[] scalars) {
        this.scalars = scalars;
    }

    // instance member method (alphabetic)

    @Override
    public void dispose() {
        final int finished = getDimension();
        for (int scalar = 0; finished != scalar; scalar++) {
            scalars[scalar].dispose();
        }
    }

    @Override
    public int getDimension() {
        return scalars.length;
    }

    @Override
    public void measure(int[] counts) {
        final int finished = getDimension();
        if (finished != counts.length) {
            throw new IllegalArgumentException("\"counts\" has a length of " + counts.length
                                               + ", but required length is "
                                               + finished);
        }

        for (int scalar = 0; finished != scalar; scalar++) {
            scalars[scalar].measure(counts[scalar]);
        }
    }

    @Override
    public float[] getRate() {
        final int finished = scalars.length;
        final float[] result = new float[finished];
        for (int scalar = 0; finished != scalar; scalar++) {
            result[scalar] = scalars[scalar].getRate();
        }
        return result;
    }

    @Override
    public TimeUnit getTimeUnit() {
        return TimeUnit.SECONDS;
    }

    @Override
    public void reset() {
        final int finished = scalars.length;
        for (int scalar = 0; finished != scalar; scalar++) {
            scalars[scalar].reset();
        }
    }
}
