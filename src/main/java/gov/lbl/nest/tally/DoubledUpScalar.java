package gov.lbl.nest.tally;

/**
 * This class wraps up two {@link ScalarMonitor} instances so that they appear
 * as a single ScalarMonitor.
 * 
 * @author patton
 */
public class DoubledUpScalar implements
                             ScalarMonitor {

    // private instance member data

    /**
     * The first ScalarMonitor wrapped by this class.
     */
    private final ScalarMonitor first;

    /**
     * The second ScalarMonitor wrapped by this class.
     */
    private final ScalarMonitor second;

    // constructors

    /**
     * Create an instance of this class.
     * 
     * @param first
     *            one of the monitors to be used by this object.
     * @param second
     *            the other monitor to be used by this object.
     */
    public DoubledUpScalar(ScalarMonitor first,
                           ScalarMonitor second) {
        this.first = first;
        this.second = second;
    }

    // instance member method (alphabetic)

    @Override
    public void dispose() {
        first.dispose();
        second.dispose();
    }

    /**
     * Returns the first ScalarMonitor wrapped by this class.
     * 
     * @return the first ScalarMonitor wrapped by this class.
     */
    public ScalarMonitor getFirst() {
        return first;
    }

    /**
     * Returns the second ScalarMonitor wrapped by this class.
     * 
     * @return the second ScalarMonitor wrapped by this class.
     */
    public ScalarMonitor getSecond() {
        return second;
    }

    @Override
    public void measure(int count) {
        first.measure(count);
        second.measure(count);
    }

    @Override
    public void reset() {
        first.reset();
        second.reset();
    }
}