package gov.lbl.nest.tally;

/**
 * This interface extends the {@link ScalarMonitor} interface to enable
 * examination of the accumulated total of a single tally point.
 * 
 * @author patton
 */
public interface ScalarSum extends
                           ScalarMonitor {
    /**
     * Returns the accumulated total of all measurements.
     * 
     * @return the accumulated total of all measurements.
     */
    long getTotal();

    /**
     * Returns the accumulated total since the last mark was invoked.
     * 
     * @return the accumulated total since the last mark was invoked.
     * @see #mark()
     */
    long getTotalSinceMark();

    /**
     * Mark this object's count.
     * 
     * @see #getTotalSinceMark()
     */
    void mark();
}