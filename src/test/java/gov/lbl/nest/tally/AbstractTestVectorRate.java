package gov.lbl.nest.tally;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.tally.MockProducer.Consumer;

/**
 * This class test that a {@link ScalarSum} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
public abstract class AbstractTestVectorRate {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The length to which to fill the {@link #SINGLE_SAWTOOTH_COUNTS}.
     */
    private static final int SINGLE_SAWTOOTH_LENGTH = 5;

    /**
     * The list of counts to use for a test's of sawtooth counts.
     */
    private static final int[] SINGLE_SAWTOOTH_COUNTS = new int[SINGLE_SAWTOOTH_LENGTH];

    static {
        int last = -1;
        for (int index = 0;
             index != SINGLE_SAWTOOTH_LENGTH;
             ++index) {
            SINGLE_SAWTOOTH_COUNTS[index] = ++last;
            if (last == 4) {
                last = -1;
            }
        }
    }

    /**
     * The length to which to fill the {@link #FAST_COUNTS}.
     */
    private static final int FAST_LENGTH = 101;

    /**
     * The list of counts to use for a test's of fast counts.
     */
    private static final int[][] FAST_COUNTS = new int[FAST_LENGTH][2];

    /**
     * The list of pause to use for a test's of fast counts.
     */
    private static final int FAST_PAUSE = 100;

    static {
        int last = -1;
        for (int index = 0;
             index != FAST_LENGTH;
             ++index) {
            FAST_COUNTS[index] = new int[] { 9,
                                             SINGLE_SAWTOOTH_COUNTS[index % SINGLE_SAWTOOTH_LENGTH] };
            if (last == 4) {
                last = -1;
            }
        }
    }

    /**
     * The length to which to fill the {@link #FAST_COUNTS}.
     */
    private static final int SLOW_LENGTH = 2 * SINGLE_SAWTOOTH_LENGTH + 1;

    /**
     * The list of counts to use for a test's of fast counts.
     */
    private static final int[][] SLOW_COUNTS = new int[SLOW_LENGTH][2];

    /**
     * The list of pause to use for a test's of fast counts.
     */
    private static final int SLOW_PAUSE = 1000;

    /**
     * The list of counts to use for a test's of fast counts.
     */
    private static final int[][] SINGLE_COUNTS = new int[SINGLE_SAWTOOTH_LENGTH][2];

    static {
        int last = -1;
        for (int index = 0;
             index != SINGLE_SAWTOOTH_LENGTH;
             ++index) {
            SINGLE_COUNTS[index] = new int[] { SINGLE_SAWTOOTH_COUNTS[index],
                                               9 };
            if (last == 4) {
                last = -1;
            }
        }
        for (int index = 0;
             index != SLOW_LENGTH;
             ++index) {
            SLOW_COUNTS[index] = SINGLE_COUNTS[index % SINGLE_SAWTOOTH_LENGTH];
        }
    }

    // private static member data

    // private instance member data

    /**
     * The {@link Consumer} instance to use in the test.
     */
    private MockProducer.Consumer consumer;

    /**
     * The {@link MockProducer} instance used in these tests.
     */
    final MockProducer producer = new MockProducer();

    /**
     * The instance being tested.
     */
    private VectorRate testObject;

    // constructors

    // instance member method (alphabetic)

    /**
     * Compares two float arrays,
     * 
     * @param message
     * @param fastRate
     * @param rate
     * @param fs
     */
    private void assertEquals(float[] fastRate,
                              float[] rate,
                              float[] fs,
                              String message) {
        if (fastRate.length != rate.length) {
            fail(message);
        }
        for (int index = 0;
             index != fastRate.length;
             ++index) {
            org.junit.jupiter.api.Assertions.assertEquals(fastRate[index],
                                                          rate[index],
                                                          fs[index],
                                                          message);
        }
    }

    /**
     * returns the expected result from 10 sec. of constant '9' counts on first
     * element and 0-4 sawtooth on second element, every 100 milliseconds.
     * 
     * @return the expected result from 10 sec. of constant '9' counts on first
     *         element and 0-4 sawtooth on second element, every 100 milliseconds.
     */
    protected abstract float[] getFastRates();

    /**
     * returns the expected result from 10 sec. of 0-4 sawtooth on first element and
     * constant '9' counts on second element, every 1000 milliseconds.
     * 
     * @return the expected result from 10 sec. of 0-4 sawtooth on first element and
     *         constant '9' counts on second element, every 1000 milliseconds.
     */
    protected abstract float[] getSlowRates();

    /**
     * Set up test environment.
     * 
     * @throws Exception when there is a problem.
     */
    @BeforeEach
    protected void setUp() throws Exception {
        final MonitorFactory factory = MonitorFactory.getMonitorFactory();
        testObject = factory.newVectorMonitor(MonitorFactory.VECTOR_RATE,
                                              2);
        consumer = new MockProducer.Consumer() {

            @Override
            public void measure(int count) {
            }

            @Override
            public void measure(int[] count) {
                testObject.measure(count);
            }
        };
    }

    /**
     * Test that a fast rate provides the correct result.
     */
    @Test
    @DisplayName("Fast Rate")
    public void testFastRate() {
        producer.produce(consumer,
                         FAST_COUNTS,
                         FAST_PAUSE);
        assertEquals(getFastRates(),
                     testObject.getRate(),
                     new float[] { 1.0F,
                                   1.0F },
                     "Failed to measure \"fast\" rates");
    }

    /**
     * Test that a constant rate provides the correct result.
     */
    @Test
    @DisplayName("Reset")
    public void testReset() {
        producer.produce(consumer,
                         SINGLE_COUNTS,
                         SLOW_PAUSE);
        testObject.reset();
        assertEquals(new float[] { 0.0F,
                                   0.0F },
                     testObject.getRate(),
                     new float[] { 0.1F,
                                   0.1F },
                     "Failed to reset");
    }

    /**
     * Test that a constant rate provides the correct result.
     */
    @Test
    @DisplayName("Saw Tooth Rate")
    public void testSawToothRate() {
        producer.produce(consumer,
                         SLOW_COUNTS,
                         SLOW_PAUSE);
        assertEquals(getSlowRates(),
                     testObject.getRate(),
                     new float[] { 0.1F,
                                   0.1F },
                     "Failed to measure \"slow\" rates");
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}
}
