package gov.lbl.nest.tally.simple;

import org.junit.jupiter.api.DisplayName;

import gov.lbl.nest.tally.AbstractTestVectorFlow;

/**
 * This class test that a {@link ScalarSumImpl} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
@DisplayName("VectorFlow class")
public class TestVectorFlowImpl extends
                                AbstractTestVectorFlow {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    protected float[] getFastRates() {
        return new float[] { 90.0F,
                             20.0F };
    }

    @Override
    protected long[] getFastTotals() {
        return new long[] { 909,
                            200 };
    }

    @Override
    protected float[] getSlowRates() {
        return new float[] { 2.0F,
                             9.0F };
    }

    @Override
    protected long[] getSlowTotals() {
        return new long[] { 20,
                            99 };
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}
}
