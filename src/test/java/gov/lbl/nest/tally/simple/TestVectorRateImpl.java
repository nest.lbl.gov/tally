package gov.lbl.nest.tally.simple;

import org.junit.jupiter.api.DisplayName;

import gov.lbl.nest.tally.AbstractTestVectorRate;

/**
 * This class test that a {@link ScalarSumImpl} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
@DisplayName("VectorRate class")
public class TestVectorRateImpl extends
                                AbstractTestVectorRate {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    protected float[] getFastRates() {
        return new float[] { 90.0F,
                             20.0F };
    }

    @Override
    protected float[] getSlowRates() {
        return new float[] { 2.0F,
                             9.0F };
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}
}
