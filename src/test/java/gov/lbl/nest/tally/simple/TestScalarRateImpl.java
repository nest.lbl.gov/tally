package gov.lbl.nest.tally.simple;

import org.junit.jupiter.api.DisplayName;

import gov.lbl.nest.tally.AbstractTestScalarRate;

/**
 * This class test that a {@link ScalarSumImpl} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
@DisplayName("ScalarRate class")
public class TestScalarRateImpl extends
                                AbstractTestScalarRate {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    protected float getFastRate() {
        return 90.0F;
    }

    @Override
    protected float getSlowRate() {
        return 2.0F;
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}
}
