package gov.lbl.nest.tally.simple;

import org.junit.jupiter.api.DisplayName;

import gov.lbl.nest.tally.AbstractTestScalarFlow;

/**
 * This class test that a {@link ScalarSumImpl} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
@DisplayName("ScalarFlow class")
public class TestScalarFlowImpl extends
                                AbstractTestScalarFlow {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    protected float getFastRate() {
        return 90.0F;
    }

    @Override
    protected float getFastTotal() {
        return 900;
    }

    @Override
    protected float getSlowRate() {
        return 2.0F;
    }

    @Override
    protected float getSlowTotal() {
        return 20;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}
}
