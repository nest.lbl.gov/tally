package gov.lbl.nest.tally.simple;

import org.junit.jupiter.api.DisplayName;

import gov.lbl.nest.tally.AbstractTestScalarSum;

/**
 * This class test that a {@link ScalarSumImpl} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
@DisplayName("VectorSum class")
public class TestVectorSumImpl extends
                               AbstractTestScalarSum {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}
}
