package gov.lbl.nest.tally;

/**
 * This class is used to "produce" a set of measurements for test classes to
 * consume.
 * 
 * @author patton
 */
public class MockProducer {

    /**
     * This interface is used to supply the method to consume produced data.
     * 
     * @author patton
     */
    public static interface Consumer {

        /**
         * Measures the produced scalar count.
         * 
         * @param count
         *            the value to measured.
         */
        void measure(int count);

        /**
         * Measures the produced vector count.
         * 
         * @param count
         *            the value to measured.
         */
        void measure(int[] count);

    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Executes the production of measurements, blocking until all measurements have
     * been consumed.
     * 
     * @param consumer
     *            the {@link Consumer} that will consume the products
     * @param counts
     *            the counts the will be produced.
     */
    public void produce(Consumer consumer,
                        int[] counts) {
        for (int count : counts) {
            consumer.measure(count);
        }
    }

    /**
     * Executes the production of measurements, blocking until all measurements have
     * been consumed.
     * 
     * @param consumer
     *            the {@link Consumer} that will consume the products
     * @param counts
     *            the counts the will be produced.
     * @param pause
     *            the pause between production of counts.
     */
    public void produce(Consumer consumer,
                        int[] counts,
                        int pause) {
        long expectedTime = System.currentTimeMillis();
        for (int count : counts) {
            consumer.measure(count);
            try {
                long now = System.currentTimeMillis();
                long correction = now - expectedTime;
                if (correction > pause) {
                    Thread.sleep(0);
                } else {
                    Thread.sleep(pause - correction);
                }
                expectedTime = expectedTime + pause;
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * Executes the production of measurements, blocking until all measurements have
     * been consumed.
     * 
     * @param consumer
     *            the {@link Consumer} that will consume the products
     * @param counts
     *            the counts the will be produced.
     * @param pauses
     *            the pauses between production of counts.
     */
    public void produce(Consumer consumer,
                        int[] counts,
                        int[] pauses) {
        for (int count : counts) {
            consumer.measure(count);
        }
    }

    /**
     * Executes the production of measurements, blocking until all measurements have
     * been consumed.
     * 
     * @param consumer
     *            the {@link Consumer} that will consume the products
     * @param counts
     *            the vectors of counts the will be produced.
     */
    public void produce(Consumer consumer,
                        int[][] counts) {

    }

    /**
     * Executes the production of measurements, blocking until all measurements have
     * been consumed.
     * 
     * @param consumer
     *            the {@link Consumer} that will consume the products
     * @param counts
     *            the vector of counts the will be produced.
     * @param pause
     *            the pause between production of counts.
     */
    public void produce(Consumer consumer,
                        int[][] counts,
                        int pause) {
        long expectedTime = System.currentTimeMillis();
        for (int[] count : counts) {
            consumer.measure(count);
            try {
                long now = System.currentTimeMillis();
                long correction = now - expectedTime;
                if (correction > pause) {
                    Thread.sleep(0);
                } else {
                    Thread.sleep(pause - correction);
                }
                expectedTime = expectedTime + pause;
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * Executes the production of measurements, blocking until all measurements have
     * been consumed.
     * 
     * @param consumer
     *            the {@link Consumer} that will consume the products
     * @param counts
     *            the vector of counts the will be produced.
     * @param pauses
     *            the pauses between production of counts.
     */
    public void produce(Consumer consumer,
                        int[][] counts,
                        int[] pauses) {

    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
