package gov.lbl.nest.tally;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.tally.MockProducer.Consumer;

/**
 * This class test that a {@link ScalarSum} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
public abstract class AbstractTestScalarSum {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The list of counts to use for a test's first set of measurements.
     */
    private static final int[] FIRST_SEQUENCE = new int[] { 1,
                                                            56,
                                                            3,
                                                            12,
                                                            9,
                                                            14 };

    /**
     * The total of the first set of measurements.
     */
    private static final int FIRST_TOTAL;

    static {
        int total = 0;
        for (int count : FIRST_SEQUENCE) {
            total += count;
        }
        FIRST_TOTAL = total;
    }

    /**
     * The list of counts to use for a test's second set of measurements.
     */
    private static final int[] SECOND_SEQUENCE = new int[] { 64,
                                                             23,
                                                             71,
                                                             5,
                                                             9,
                                                             13,
                                                             45,
                                                             23 };

    /**
     * The total of the second set of measurements.
     */
    private static final int SECOND_TOTAL;

    static {
        int total = 0;
        for (int count : SECOND_SEQUENCE) {
            total += count;
        }
        SECOND_TOTAL = total;
    }

    /**
     * The list of counts to test a single measurement.
     */
    private static final int[] SINGLE_COUNT_ARRAY = new int[] { 12 };

    // private static member data

    // private instance member data

    /**
     * The {@link Consumer} instance to use in the test.
     */
    private MockProducer.Consumer consumer;

    /**
     * The {@link MockProducer} instance used in these tests.
     */
    final MockProducer producer = new MockProducer();

    /**
     * The instance being tested.
     */
    private ScalarSum testObject;

    // constructors

    // instance member method (alphabetic)

    /**
     * Set up test environment.
     * 
     * @throws Exception when there is a problem.
     */
    @BeforeEach
    protected void setUp() throws Exception {
        final MonitorFactory factory = MonitorFactory.getMonitorFactory();
        testObject = factory.newScalarMonitor(MonitorFactory.SCALAR_SUM);
        consumer = new MockProducer.Consumer() {

            @Override
            public void measure(int count) {
                testObject.measure(count);
            }

            @Override
            public void measure(int[] count) {
            }
        };
    }

    /**
     * Test that one set of measurements followed by another set provide the correct
     * result.
     */
    @Test
    @DisplayName("Continued Measuring")
    public void testContinuedMeasuring() {
        testMeasuring();
        producer.produce(consumer,
                         SECOND_SEQUENCE);
        assertEquals(FIRST_TOTAL + SECOND_TOTAL,
                     testObject.getTotal(),
                     "Failed to measure cumulated set of counts");
    }

    /**
     * Test that one set of measurements that follow a mark after an set provide the
     * correct result.
     */
    @Test
    @DisplayName("Marked Measuring")
    public void testMarkedMeasuring() {
        testMeasuring();
        assertEquals(FIRST_TOTAL,
                     testObject.getTotalSinceMark(),
                     "Marked total does not match complete total");
        testObject.mark();
        assertEquals(FIRST_TOTAL,
                     testObject.getTotal(),
                     "Mark failed to preserved complete total");
        assertEquals(0,
                     testObject.getTotalSinceMark(),
                     "Mark has not zeroed marked total");
        producer.produce(consumer,
                         SECOND_SEQUENCE);
        assertEquals(FIRST_TOTAL + SECOND_TOTAL,
                     testObject.getTotal(),
                     "Failed to measure cumulated set of counts");
        assertEquals(SECOND_TOTAL,
                     testObject.getTotalSinceMark(),
                     "Mark failed to measure limited total");
    }

    /**
     * Test that a set of measurements provide the correct result.
     */
    @Test
    @DisplayName("Measuring")
    public void testMeasuring() {
        producer.produce(consumer,
                         FIRST_SEQUENCE);
        assertEquals(FIRST_TOTAL,
                     testObject.getTotal(),
                     "Failed to measure first set of counts");
    }

    /**
     * Test that reseting the test object works correctly.
     */
    @Test
    @DisplayName("Reset")
    public void testReset() {
        testMeasuring();
        testObject.reset();
        assertEquals(0,
                     testObject.getTotal(),
                     "Reset has not zeroed complete total");
        assertEquals(0,
                     testObject.getTotalSinceMark(),
                     "Reset has not zeroed marked total");
        producer.produce(consumer,
                         SECOND_SEQUENCE);
        assertEquals(SECOND_TOTAL,
                     testObject.getTotal(),
                     "Failed to measure compelete total after reset");
    }

    /**
     * Test that a single measurement provides the correct result.
     */
    @Test
    @DisplayName("Single Measurement")
    public void testSingleMeasurement() {
        producer.produce(consumer,
                         SINGLE_COUNT_ARRAY);
        assertEquals(SINGLE_COUNT_ARRAY[0],
                     testObject.getTotal(),
                     "Failed to measure single count");
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}
}
