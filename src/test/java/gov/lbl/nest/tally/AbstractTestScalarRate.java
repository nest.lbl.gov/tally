package gov.lbl.nest.tally;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.tally.MockProducer.Consumer;

/**
 * This class test that a {@link ScalarSum} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
public abstract class AbstractTestScalarRate {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The length to which to fill the {@link #SINGLE_SAWTOOTH_COUNTS}.
     */
    private static final int SINGLE_SAWTOOTH_LENGTH = 5;

    /**
     * The list of counts to use for a test's of sawtooth counts.
     */
    private static final int[] SINGLE_SAWTOOTH_COUNTS = new int[SINGLE_SAWTOOTH_LENGTH];

    static {
        int last = -1;
        for (int index = 0;
             index != SINGLE_SAWTOOTH_LENGTH;
             ++index) {
            SINGLE_SAWTOOTH_COUNTS[index] = ++last;
            if (last == 4) {
                last = -1;
            }
        }
    }

    /**
     * The length to which to fill the {@link #FAST_COUNTS}.
     */
    private static final int FAST_LENGTH = 101;

    /**
     * The list of counts to use for a test's of constant counts.
     */
    private static final int[] FAST_COUNTS = new int[FAST_LENGTH];

    /**
     * The list of pause to use for a test's of constant counts.
     */
    private static final int CONSTANT_PAUSE = 100;

    static {
        for (int index = 0;
             index != FAST_LENGTH;
             ++index) {
            FAST_COUNTS[index] = 9;
        }
    }

    /**
     * The length to which to fill the {@link #SLOW_COUNTS}.
     */
    private static final int SLOW_LENGTH = 2 * SINGLE_SAWTOOTH_LENGTH + 1;

    /**
     * The list of counts to use for a test's of constant counts.
     */
    private static final int[] SLOW_COUNTS = new int[SLOW_LENGTH];

    /**
     * The list of pause to use for a test's of constant counts.
     */
    private static final int SLOW_PAUSE = 1000;

    static {
        for (int index = 0;
             index != SLOW_LENGTH;
             ++index) {
            SLOW_COUNTS[index] = SINGLE_SAWTOOTH_COUNTS[index % SINGLE_SAWTOOTH_LENGTH];
        }
    }

    // private static member data

    // private instance member data

    /**
     * The {@link Consumer} instance to use in the test.
     */
    private MockProducer.Consumer consumer;

    /**
     * The {@link MockProducer} instance used in these tests.
     */
    final MockProducer producer = new MockProducer();

    /**
     * The instance being tested.
     */
    private ScalarRate testObject;

    // constructors

    // instance member method (alphabetic)

    /**
     * returns the expected result from 10 sec. of constant '9' counts every 100
     * milliseconds.
     * 
     * @return the expected result from 10 sec. of constant '9' counts every 100
     *         milliseconds.
     */
    protected abstract float getFastRate();

    /**
     * returns the expected result from 10 sec. of sawtooth of 0 to 4 over 5 sec.
     * for 10 Sec.
     * 
     * @return the expected result from 10 sec. of sawtooth of 0 to 4 over 5 sec.
     *         for 10 Sec.
     */
    protected abstract float getSlowRate();

    /**
     * Set up test environment.
     * 
     * @throws Exception when there is a problem.
     */
    @BeforeEach
    protected void setUp() throws Exception {
        final MonitorFactory factory = MonitorFactory.getMonitorFactory();
        testObject = factory.newScalarMonitor(MonitorFactory.SCALAR_RATE);
        consumer = new MockProducer.Consumer() {

            @Override
            public void measure(int count) {
                testObject.measure(count);
            }

            @Override
            public void measure(int[] count) {
            }
        };
    }

    /**
     * Test that a fast rate provides the correct result.
     */
    @Test
    @DisplayName("Fast Rate")
    public void testFastRate() {
        producer.produce(consumer,
                         FAST_COUNTS,
                         CONSTANT_PAUSE);
        assertEquals(getFastRate(),
                     testObject.getRate(),
                     1.0F,
                     "Failed to measure \"fast\" rate");
    }

    /**
     * Test that a reset provides the correct result.
     */
    @Test
    @DisplayName("Reset")
    public void testReset() {
        producer.produce(consumer,
                         SINGLE_SAWTOOTH_COUNTS,
                         SLOW_PAUSE);
        testObject.reset();
        assertEquals(0.0F,
                     testObject.getRate(),
                     0.01F,
                     "Failed to reset");
    }

    /**
     * Test that a slow rate provides the correct result.
     */
    @Test
    @DisplayName("Slow Rates")
    public void testSlowRate() {
        producer.produce(consumer,
                         SLOW_COUNTS,
                         SLOW_PAUSE);
        assertEquals(getSlowRate(),
                     testObject.getRate(),
                     0.2F,
                     "Failed to measure \"slow\" rate");
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}
}
